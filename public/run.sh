# increment blog number & store that info

z=$(cat blog.number)
let z=z+1
rm blog.number
echo $z >> blog.number

# get data

echo date
read date
echo title
read title
echo message
read message

# insert data

sed '/Feed-->/a <hr><h2>'"$z"'. '"$title"'</h2><p>'"$message"'</p>' universe.html >> universe2.html
sed '/Feed-->/a <item><title>'"$z"'. '"$title"'</title><pubDate>'"$date"'</pubDate><description>'"$message"'</description></item>' universe.xml >> universe2.xml
rm universe.*
cat universe2.html >> universe.html
cat universe2.xml >> universe.xml
rm universe2*
